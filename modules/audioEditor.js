/**
 * Created by מרדכי on 12 אוגוסט 2016.
 */

var ffmpeg = require('fluent-ffmpeg');
var logger = require('./logger');
var fs = require('fs');
var defer = require('./defer')

var filetype = require('file-type');
var readChunk = require('read-chunk');

function isMp3(filePath) {
    var buffer = readChunk.sync(filePath, 0, 4100);
    var inputFileExt = filetype(buffer).ext;
	return inputFileExt === 'mp3';
}

function convertToMp3(file, path) {
    var deferred = defer.Deferred();
    var newPath = path + '.mp3';
    
    if (isMp3(path)) {
        fs.renameSync(path, newPath);
        file.path = newPath;
        deferred.resolve(newPath);
        return deferred.promise;
    }

    logger.debug(`Converting ${path} to ${newPath}`)

    ffmpeg(path)
        .format('mp3')
        .on('end', function () {
            logger.debug('Successfully Converted ' + path + ' to mp3');

            fs.unlink(path, function (err) {
                if (err)
                    logger.error("Failed to remove remnants of old file, Reason: " + err);
            });

            var name = JSON.parse(file.name);
            name.fileExtension = '.mp3';
            file.name = JSON.stringify(name);

            file.path = newPath;
            deferred.resolve(newPath);
        })
        .on('error', function (err) {
            logger.debug("Failed to convert to mp3, Reason: " + err);

            fs.stat(newPath, function (err, stats) {
                if (!err)
                    fs.unlink(newPath, function (err) {
                        if (err)
                            logger.debug("Failed to remove remnants of mp3, Reason: " + err);
                    });
            });

            deferred.resolve(path);
        })
        .save(newPath);
    
    return deferred.promise;
}

module.exports.convertToMp3 = convertToMp3;
