/**
 * Created by רוני on 30 אפריל 2020.
 */

var fs = require('fs');

function isReady(file, folder) {
    if (file.previous === '') {
        return true;
    }
    var files = fs.readdirSync(folder);
    if (files.length === 0) {
        return false;
    }
    files.sort((a, b) => parseInt(a.split('.')[0]) - parseInt(b.split('.')[0]))
    let [lastFile] = files.slice(-1);
    let fileRegex = /^(?<num>\d+.?)(?<name>.+)(?<ext>\..+)$/mg;
    let fileName = fileRegex.exec(lastFile).groups.name;
    if (fileName.slice(1, fileName.length) === file.previous) {
        return true
    }
    return false;
}

function isEarlier(file, folder) {
    var files = fs.readdirSync(folder);
    if (files.length === 0) {
        return false;
    }
    files.sort((a, b) => parseInt(a.split('.')[0]) - parseInt(b.split('.')[0]))
    prev = false;
    for (let i = 0; i < files.length; i++) {    
        let current = files[i]
        let fileRegex = /^(?<num>\d+.?)(?<name>.+)(?<ext>\..+)$/mg;
        let fileName = fileRegex.exec(current).groups.name;
        if (prev) {
            if (fileName.slice(1, fileName.length) === file.subject) {
                return true;
            }
            prev = false;
        }
        if (fileName.slice(1, fileName.length) === file.previous) {
            prev = true;
            continue;
        }
    }
}
module.exports.isReady = isReady;
module.exports.isEarlier = isEarlier;