/**
 * Created by רוני on 16 ינואר 2020.
 */

function Deferred() {
    var result = {};
    result.promise = new Promise(function(resolve, reject) {
        result.resolve = resolve;
        result.reject = reject;
    });
    return result;
};

module.exports.Deferred = Deferred;