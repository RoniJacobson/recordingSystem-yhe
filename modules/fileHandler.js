/**
 * Created by מרדכי on 10 אוגוסט 2016.
 */

var formidableBuilder = require('./formidableBuilder');
var defer = require('./defer');


function handleFile(request) {
    var deferred = defer.Deferred();
    
    var form = formidableBuilder.buildForm(deferred);
    form.parse(request);

    return deferred.promise;
}

module.exports.handleFile = handleFile;