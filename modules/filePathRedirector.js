/**
 * Created by מרדכי on 01 ספטמבר 2016.
 */

var config = require('../config');
var fs = require('fs');
var logger = require('./logger');
var defer = require('./defer')
var nextTrackNumberProvider = require('./nextTrackNumberProvider');
var randomNumberGenerator = require('../modules/randomNumberGenerator');
var checkReady = require('../modules/checkReady.js')
let sleep = require('util').promisify(setTimeout);

async function redirectFilePath(file) {
    var deferred = defer.Deferred();
    try {
        let {redirectedPath, trackNumber} = await getRedirectedFilePath(file)
        fs.renameSync(file.path, redirectedPath);
        logger.debug('File path redirected successfully. file: ' + redirectedPath);
        deferred.resolve({redirectedPath, trackNumber});
    }
    catch (error){
        logger.debug('File path is not redirected, do not now why..');
        deferred.reject(error);
    }

    return deferred.promise;
}

async function getRedirectedFilePath(file) {
    var deferred = defer.Deferred();


    var RecordingInfo = JSON.parse(file.name);
    var folder = config.uploadsFolder + RecordingInfo.path;
    let counter = 0;
    while (!checkReady.isReady(RecordingInfo, folder)) {
        await sleep(1000);
        if (counter++ >= 45) {
            if (checkReady.isEarlier(RecordingInfo, folder)) {
                fs.unlinkSync(file.path);
                deferred.reject(Error('the file was attempted to be uploaded twice for some reason'));
                return deferred.promise;
            }
            counter = 0;
        }
    }
    var trackNumber = await nextTrackNumberProvider.getNextTrackNumberInFolder(folder, RecordingInfo.subject);
    var filePath = config.uploadsFolder + RecordingInfo.path + '/' + trackNumber + '. ' + RecordingInfo.subject;
    fs.stat(filePath, function (err, stat) {
        if (err == null) { // File exists already! Adding a random number to the file name to avoid conflict.
            filePath += randomNumberGenerator.generateRandomNumber();
            logger.debug('Adding a random number to the file name to avoid conflict between duplicate file names.');
        } else if (err.code == 'ENOENT') { // File does not exist
        } else {
            deferred.reject(err);
        }
        redirectedPath = filePath;
        deferred.resolve({redirectedPath, trackNumber});
    });

    return deferred.promise;
}

module.exports.redirectFilePath = redirectFilePath;
