/**
 * Created by מרדכי on 10 אוגוסט 2016.
 */

var amazonUploader = require('./amazonUploader');
var config = require('../config');
var formidable = require('formidable');
var filePathRedirector = require('./filePathRedirector');
var metadataHandler = require('./metadataHandler');
var audioEditor = require('./audioEditor');

function buildForm(promise) {
    var form = new formidable.IncomingForm();

    form.uploadDir = config.uploadsFolder;
    form.on('file', async function (field, file) {
        try {
            let {redirectedPath, trackNumber} = await filePathRedirector.redirectFilePath(file);
            redirectedPath = await audioEditor.convertToMp3(file, redirectedPath);
            filePath = await metadataHandler.addMetadataToFile(file, redirectedPath, trackNumber);
            await amazonUploader.upload(filePath);
            promise.resolve(filePath);
        } catch (error) {
            promise.reject(error);
        }
    });
    form.on('error', function (err) {
        promise.reject(err);
    });

    return form;
}

module.exports.buildForm = buildForm;
