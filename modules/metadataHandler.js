/**
 * Created by מרדכי on 12 אוגוסט 2016.
 */

var config = require('../config');
var NodeID3 = require('node-id3')
var readChunk = require('read-chunk');
var filetype = require('file-type');
var logger = require('./logger');
var path = require('path');
var defer = require('./defer');

function addMetadataToFile(file, filePath, trackNumber) {
    var deferred = defer.Deferred();
    var buffer = readChunk.sync(filePath, 0, 4100);
    var inputFileExt = filetype(buffer).ext;    
	if (inputFileExt !== 'mp3' || config.metadata.applyModule == false) {
        logger.debug('Metadata is not written, due to wrong file extension OR do to false configuration.');
		deferred.resolve(filePath);
    } else {
        var recordingInfo = JSON.parse(file.name);
        try {
            var metadata = {
                artist: recordingInfo.lecturer,
                trackNumber: trackNumber,
                date: config.metadata.currentYear,
                album: path.dirname(filePath).split('/').pop()
            };
            
            NodeID3.write(metadata, filePath, function(error, buffer) { 
                if (error) {
                    deferred.reject(error);
                }
                else {
                    logger.debug('Metadata written successfully. file path: ' + filePath + '; metadata: ' + JSON.stringify(metadata));
                    deferred.resolve(filePath);
                }
            }) 
        } catch (error) {
            logger.error("Error while writing metadata" + error.message);
        }
    }

    return deferred.promise;
}

module.exports.addMetadataToFile = addMetadataToFile;
