/**
 * Created by מרדכי on 14 אוגוסט 2016.
 */

var config = require('../config');
var fs = require('fs');
var defer = require('./defer');

function getNextTrackNumberInFolder(folderName, filename) {
    var deferred = defer.Deferred();

    var files = fs.readdirSync(folderName);
    var maxTrackNumber = config.metadata.minimumTrackNumber;
    if (files.length === 0) {
        deferred.resolve(formatTrackNumber(maxTrackNumber));
    }
    var index = 0;
    for (var i = 0; i < files.length; i++) {
        (function (file) {
            var currentTrackNumber = parseInt(file.split('.')[0]);
            if (maxTrackNumber <= currentTrackNumber && file !== filename) {
                maxTrackNumber = currentTrackNumber + 1;
            }

            if (index++ === files.length - 1) {
                deferred.resolve(formatTrackNumber(maxTrackNumber));
            }
        }) (files[i]);
    }

    return deferred.promise;
}

function formatTrackNumber(trackNumber) {
    if(trackNumber.toString().length == 1) {
        return '0' + trackNumber;
    } else {
        return trackNumber;
    }
}

module.exports.getNextTrackNumberInFolder = getNextTrackNumberInFolder;