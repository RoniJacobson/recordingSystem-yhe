/**
 * Created by אביה on 7 יולי 2019.
 */

var config = require('../config');
var treeScanner = require('../treeScanner');
var logger = require('./logger');
var chokidar = require('chokidar');
var fs = require('fs');
var fsPath = require('path');
var q = require('q');

function init() {
    const watcher = chokidar.watch(config.uploadsFolder, {
        pesistent: true,
        ignoreInitial: true,
        ignorePermissionErrors: true,
        ignored: (path, stats) => {
            var result = false;
            if (stats)
                result = !stats.isDirectory();

            return result;
        }
    });

    watcher
        .on('addDir', path => refreshInAMoment())
        .on('unlinkDir', path => refreshInAMoment());
}

var timer;

function refreshInAMoment() {
    if (timer)
        clearTimeout(timer);

    timer = setTimeout(() => {
        refreshFolderList()
    }, config.dirScanner.refreshDelay);
}

function JstreePath() {
    return fsPath.resolve(fsPath.dirname(__dirname), config.dirScanner.outFile)
}

function refreshFolderList() {
    tree = treeScanner.getTree()
    fs.writeFile(JstreePath(),
				 JSON.stringify(tree),
				 'utf8',
				 (err) => {
				     if (err) {
				         logger.error('Error while updating the tree. Error message: ' + err.message);
				     }
				     logger.info('Dir tree successfully updated');
				 })
}

function getJstreeData() {
    var deferred = q.defer();

    fs.readFile(JstreePath(), 'utf8', function (err, data) {
        if (err) {
            logger.error('Error while reading folder structure. Error message: ' + err.message);
            deferred.reject(err);
        } else {
            jsonString = data;
            jsonString = jsonString.replace(/^(\uFEFF|\uEFBBBF)/, '')  // http://stackoverflow.com/a/24376813
            deferred.resolve(JSON.parse(jsonString))
        }
    })

    return deferred.promise;
}

module.exports.start = init;
module.exports.refreshFolderList = refreshFolderList;
module.exports.getJstreeData = getJstreeData;