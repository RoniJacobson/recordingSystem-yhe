/**
 * Created by אביה on 07 ספטמבר 2018.
 */

app.controller('monitoringSystemController', ['$scope', 'config', '$http', '$timeout', function ($scope, config, $http, $timeout) {
    $scope.homeWelcomeSentence = config.strings.monitoringWelcomeSentence;
    
    $http.get(config.serverApi.getYears)
            .then(function (response) {
                $scope.years = response.data.years;
                $scope.currentYear = response.data.currentYear;
            });

    function refreshAnomalies(path) {
        function getAnomaly(url, scopeModel) {
            $scope[scopeModel] = { loading: true, data: null };
            $http.get(url, { params: { path: path } })
                .then(response => $scope[scopeModel].data = response.data)
                .finally(() => $scope[scopeModel].loading = false);
        }
        getAnomaly(config.serverApi.getNonMp3Files, 'nonMp3Files');
        getAnomaly(config.serverApi.getDuplicatedFiles, 'duplicatedFiles');
        getAnomaly(config.serverApi.getOutOfOrderFiles, 'outOfOrderFiles');
        getAnomaly(config.serverApi.getFilesWithDifferentArtist, 'filesWithDifferentArtist');
        getAnomaly(config.serverApi.getInactive, 'inactiveLessons');
    };

    $scope.$watch('searchContext', function (newValue, oldValue) {
        if (newValue === "כל המאגר")
            refreshAnomalies('/');
        else if (newValue !== undefined)
            refreshAnomalies('/' + newValue + '/');
    });
}]);