/**
 * Created by מרדכי on 29 יוני 2016.
 */

app.controller('recordingSystemController', ['$scope', 'uploadFileService', 'recordingPackageBuilderService', 'config', function ($scope, uploadFileService, recordingPackageBuilderService, config) {
    $scope.homeWelcomeSentence = config.strings.homeWelcomeSentence;
    $scope.lecturers = config.lecturers.sort();
    $scope.recordingLecturer = 'אחר';
    $scope.lecturers.unshift('אחר');
    $scope.chooseFolderText = config.strings.chooseFolderText;
    $scope.uploadingModalProgress = '0%';
    $scope.sortableConf = {
        animation: 150,
        handle: '.handle'
    };
    $scope.whatRemove = {
        leadingnumbers : false,
    };
    $scope.numberUploaded = 0;

    $scope.checkboxChanged = function() {
        $scope.recordings.forEach(recording => {
            recording.name = nameRecording(recording.fullname);
        });
    }

    function nameRecording(name){
        if ($scope.whatRemove.leadingnumbers) {
            var regex = /^(?<num>\d+.?)(?<name>.+)(?<ext>\..+)$/mg;
            var res = regex.exec(name);
            if (res != null) {
                return res.groups.name.trim();
            }
        }
        var regex = /^(?<name>.+)(?<ext>\..+)$/mg
        var res = regex.exec(name);
        if (res != null) {
            return res.groups.name.trim();
        }
        return name.trim();
    }
    
    $scope.newRecording = function (recording) {
        if ($scope.recordings[0].recording !== null)
            $scope.recordings.push({
                recording: recording,
                name: nameRecording(recording.name),
                uploadingModalProgress: '0%',
                uploadingModalText: '',
                fullname: recording.name,
                previous: ''
            })
        else {
            $scope.recordings[0].recording = recording;
            $scope.recordings[0].name = nameRecording(recording.name);
            $scope.recordings[0].fullname = recording.name;
        }
    };

    $scope.recordings = [{
        recording: null,
        name: '',
        uploadingModalProgress: '0%',
        uploadingModalText: '',
        fullname: '',
        previous: ''
    }]

    var valid = /^((?![\\/:?\"<>\|]).)*$/i; // Doesn't contain the characters: \/:?"<>|
    $scope.$watch('recordingSubject', function (newValue, oldValue) {
        if(!valid.test(newValue)) {
            $scope.recordingSubject = oldValue;
        }
    });

    $scope.uploadRecording = function () {
        if($scope.recordings.map(validateRecording).includes(false)) return;

        for (var i=1; i<$scope.recordings.length;i++) {
            $scope.recordings[i].previous = $scope.recordings[i - 1].name;
            $scope.recordings[i].uploadingModalText = config.strings.finalText;
        }

        for (var i=0; i<$scope.recordings.length;i++) {
            var recordingPackage = recordingPackageBuilderService.buildRecordingPackage($scope, $scope.recordings[i]);
            uploadFileService.uploadFile(config.serverApi.uploadFile, recordingPackage, onProgressCallBack, $scope.recordings[i]);
        }
    };

    $scope.deleteRecording = function (index) {
        if ($scope.recordings.length === 1){
            $scope.recordings[0].recording = null;
            $scope.recordings[0].name = '';
        }
        else {
            $scope.recordings.splice(index, 1)
        }
    }

    function validateRecording(recording) {
        if(!recording.recording || !recording.name) {
            recording.uploadingModalText = config.strings.badRecordInfoText;
        } else if(!$scope.pathResult || $scope.pathResult == '/false') {
            recording.uploadingModalText = config.strings.noPathEntered;
        } else {
            recording.uploadingModalText = config.strings.okText;
            return true;
        }
        return false;
    }

    function onProgressCallBack(recording, evt) {
        if (evt.loaded === evt.total) {
            recording.uploadingModalProgress = '100%';
            recording.uploadingModalText = config.strings.uploadingSuccessText;
            $scope.numberUploaded++;
        }
        else {
            recording.uploadingModalText = config.strings.uploadingInProcessText;
            recording.uploadingModalProgress = Math.round(evt.loaded / evt.total * 100) + '%';
        }
        $scope.$apply();
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}]);