/**
 * Created by מרדכי on 28 אוגוסט 2016.
 */

app.directive('modal', function () {
    return {
        restrict: 'E',
        templateUrl: '../partials/modal.html',
        scope: false,
        controller: ['$scope', 'config', function ($scope, config) {
            $("#uploadingModal").on("hidden.bs.modal", function () {
                if (!$scope.recordings.map(recording => recording.uploadingModalText === config.strings.uploadingSuccessText).includes(false)) {
                    location.reload();
                }
            });
        }]
    };
});