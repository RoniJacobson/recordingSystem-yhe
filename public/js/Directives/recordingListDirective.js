/**
 * Created by רוני on 06 פברואר 2020.
 */

app.directive("recordingList", function() {
    return {
        restrict: 'E',
        templateUrl: '../partials/recordingList.html'
    };
});