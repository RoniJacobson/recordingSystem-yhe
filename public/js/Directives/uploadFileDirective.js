/**
 * Created by מרדכי on 06 יולי 2016.
 */

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            
            element.bind('change', function () {
                var files = element[0].files;
                for (var i=0; i<files.length; i++)
                    scope.newRecording(files[i]);
                element.val(null);
                scope.$apply();
            });
        }
    };
}]);