/**
 * Created by מרדכי on 30 אוגוסט 2016.
 */

app.service('recordingPackageBuilderService', function () {
    this.buildRecordingPackage = function(scope, recording) {
        var fileExtension = '.' + getFileExtension(recording.recording.name);
        var RecordingInfo = {
            subject: recording.name,
            lecturer: scope.recordingLecturer,
            path: scope.pathResult,
            fileExtension: fileExtension,
            previous: recording.previous
        };
        var formData = new FormData();
        formData.append('file', recording.recording, JSON.stringify(RecordingInfo));

        return formData;
    };

    function getFileExtension(fileName) {
        return fileName.split('.').pop();
    }
});